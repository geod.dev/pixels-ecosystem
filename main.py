import sys
from random import randint

import pygame


def reproduce(top, left):
    luck = randint(0, 100)
    if luck < 50:
        return False
    else:
        pos = get_side_pos(top, left)


def get_side_pos(top, left):
    return randint(top - 1, top + 1), randint(left - 1, left + 1)


# Initialize Pygame
pygame.init()
size = width, height = 1000, 1000
screen = pygame.display.set_mode(size)
pygame.display.set_caption("EcoSys")

pixels = [(width / 2, height / 2)]
font = pygame.font.SysFont('Comic Sans MS', 30)
turns = 0
tests = 1

while True:

    screen.fill((0, 0, 0))
    if len(pixels) == 0:
        turns = 0
        tests += 1
        pixels.append((width / 2, height / 2))

    pixs = pixels.copy()
    for pixel in pixs:
        pygame.draw.line(screen, (255, 255, 255), pixel, pixel)
        new_pix = reproduce(int(pixel[0]), int(pixel[1]))
        if new_pix:
            pixels.append(new_pix)
            pygame.draw.line(screen, (255, 0, 0), new_pix, new_pix)
        else:
            pixels.remove(pixel)

    turns += 1
    turns_text = font.render(f"{turns}", False, (255, 255, 255))
    screen.blit(turns_text, (0, 0))

    tests_text = font.render(f"{tests}", False, (255, 255, 255))
    screen.blit(tests_text, (width - tests_text.get_width(), 0))

    pygame.display.flip()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()
